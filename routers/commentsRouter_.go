package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"],
        beego.ControllerComments{
            Method: "GetOneAdminUsrsInfo",
            Router: "/admin/userinfo/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"],
        beego.ControllerComments{
            Method: "AddAdminUsrsInfo",
            Router: "/admin/userinfo/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"],
        beego.ControllerComments{
            Method: "DeleteAdminUsrsInfo",
            Router: "/admin/userinfodelete/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"],
        beego.ControllerComments{
            Method: "EditAdminUsrsInfo",
            Router: "/admin/userinfoedit/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:AdminUsrInfoController"],
        beego.ControllerComments{
            Method: "GetAdminUsrsInfo",
            Router: "/admin/usersinfo",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "DeleteArticle",
            Router: "/admin/artdelete/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "EditArticle",
            Router: "/admin/article/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "AddArtcile",
            Router: "/admin/article/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "GetArtcile",
            Router: "/article/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "GetArtciles",
            Router: "/articles",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:ArticleController"],
        beego.ControllerComments{
            Method: "GetCateArt",
            Router: "/category/:id/artlist",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"],
        beego.ControllerComments{
            Method: "DeleteCategory",
            Router: "/admin/catedelete/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"],
        beego.ControllerComments{
            Method: "EditCategory",
            Router: "/admin/category/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"],
        beego.ControllerComments{
            Method: "AddCategory",
            Router: "/admin/category/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"],
        beego.ControllerComments{
            Method: "GetCategories",
            Router: "/categories",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:CategoryController"],
        beego.ControllerComments{
            Method: "GetCategory",
            Router: "/category/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:LoginController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:LoginController"],
        beego.ControllerComments{
            Method: "AdminLoginIndex",
            Router: "/admin/login",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:LoginController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:LoginController"],
        beego.ControllerComments{
            Method: "AdminLogin",
            Router: "/admin/login",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "GetAllUsers",
            Router: "/admin/allusers",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "ChangePwd",
            Router: "/admin/changepwd/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "GetUser",
            Router: "/admin/user/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "ResetPwd",
            Router: "/admin/user/:id/reset",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "AddUser",
            Router: "/admin/user/add",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "DeleteUser",
            Router: "/admin/userdelete/:id",
            AllowHTTPMethods: []string{"delete"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "EditUser",
            Router: "/admin/useredit/:id",
            AllowHTTPMethods: []string{"put"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"] = append(beego.GlobalControllerRouter["beegoProject/controllers/admin:UserController"],
        beego.ControllerComments{
            Method: "GetUsers",
            Router: "/admin/users",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
