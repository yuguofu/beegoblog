package routers

import (
	"beegoProject/controllers"
	"beegoProject/controllers/admin"
	"github.com/astaxie/beego"
	"html/template"
	"net/http"
)

// 404页面重写
func page_not_found(rw http.ResponseWriter, r *http.Request){
	t,_:= template.New("404.html").ParseFiles(beego.BConfig.WebConfig.ViewsPath+"/404.html")
	data :=make(map[string]interface{})
	data["content"] = "page not found"
	_ = t.Execute(rw, data)
}

// 500页面重写
func err500(rw http.ResponseWriter, r *http.Request){
	t,_:= template.New("500.html").ParseFiles(beego.BConfig.WebConfig.ViewsPath+"/500.html")
	data :=make(map[string]interface{})
	data["content"] = "server error"
	_ = t.Execute(rw, data)
}


func init() {
	// 注册错误处理控制器
	// beego.ErrorController(&controllers.ErrorController{})

	beego.ErrorHandler("404",page_not_found)	//404处理
	beego.ErrorHandler("500",err500)	//500处理

	// 注册IndexController
	beego.Include(&controllers.IndexController{})
	// 注册后台登录路由
	beego.Include(&admin.LoginController{})
	// 注册后台文章管理路由
	beego.Include(&admin.ArticleController{})
	// 注册分类路由
	beego.Include(&admin.CategoryController{})
	// 注册用户管理路由
	beego.Include(&admin.UserController{})
	//注册管理员信息管理路由
	beego.Include(&admin.AdminUsrInfoController{})

	// 注册前台文章展示路由
	beego.Include(&controllers.ArticleDetailController{})
}
