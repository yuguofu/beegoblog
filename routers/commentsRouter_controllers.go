package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context/param"
)

func init() {

    beego.GlobalControllerRouter["beegoProject/controllers:ArticleDetailController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:ArticleDetailController"],
        beego.ControllerComments{
            Method: "ArticleDetail",
            Router: "/home/article/:id",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "Index",
            Router: "/",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "About",
            Router: "/home/about",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "Archive",
            Router: "/home/archive",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "Article",
            Router: "/home/article",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "Article",
            Router: "/home/article",
            AllowHTTPMethods: []string{"post"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

    beego.GlobalControllerRouter["beegoProject/controllers:IndexController"] = append(beego.GlobalControllerRouter["beegoProject/controllers:IndexController"],
        beego.ControllerComments{
            Method: "CateArtList",
            Router: "/home/category/:id/artlist",
            AllowHTTPMethods: []string{"get"},
            MethodParams: param.Make(),
            Filters: nil,
            Params: nil})

}
