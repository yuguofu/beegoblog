# beegoblog

#### 介绍
beego博客项目，前台展示使用模板渲染，后台管理使用vue cli  

预览地址：  
~~http://47.108.27.128:8080/~~  
阿里云服务器到期，现使用内网穿透，访问地址： http://f3.ttkt.cc:51465/


#### 软件架构
1. Go + beego + layui + vue2 + MySQL8  
2. 前台使用 不落阁 模板，连接： [https://gitee.com/LY2016Start/blogtemplet](https://gitee.com/LY2016Start/blogtemplet)  
3. 后台管理项目地址：[https://gitee.com/yuguofu/beego_project_admin.git](https://gitee.com/yuguofu/beego_project_admin.git)


#### 安装教程
本应用基于golang语言的beego框架开发,在确保安装了golang环境  
1.  克隆到本地  
2.  安装go.mod中依赖 `go mod tidy`  
3.  编译运行  
    `bee run`  
4.  打包 `go build`  


#### 使用说明
数据库连接信息等请在 app.conf 中配置

#### 