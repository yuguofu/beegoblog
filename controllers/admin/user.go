package admin

import (
	"beegoProject/models"
	"beegoProject/utils/errmsg"
	"beegoProject/utils/validator"
	"encoding/json"
	"github.com/astaxie/beego"
)

type UserController struct {
	beego.Controller
}

//获取单个用户信息
// @router /admin/user/:id [get]
func (this *UserController) GetUser() {
	id, _ := this.GetInt(":id")
	userInfo, code := models.GetUserInfo(id)

	result := map[string]interface{}{
		"status":  code,
		"data":    userInfo,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result

	this.ServeJSON() //返回json格式
}

//用户列表
// @router /admin/users [get]
func (this *UserController) GetUsers() {
	pageSize, err := this.GetInt("pagesize")
	if pageSize < 1 || err != nil {
		pageSize = 10
	}
	pageNum, err := this.GetInt("pagenum")
	if pageNum < 1 || err != nil {
		pageNum = 1
	}

	users, total := models.GetUsers("", pageSize, pageNum)
	code := errmsg.SUCCSE

	result := map[string]interface{}{
		"status":  code,
		"data":    users,
		"total":   total,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}


//获取所有用户
// @router /admin/allusers [get]
func (this *UserController) GetAllUsers() {

	allusers, total := models.GetAllUsers()
	code := errmsg.SUCCSE

	result := map[string]interface{}{
		"status":  code,
		"data":    allusers,
		"total":   total,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}



//删除用户
// @router /admin/userdelete/:id [delete]
func (this *UserController) DeleteUser() {
	id, _ := this.GetInt(":id")

	code := models.DeleteUser(id)
	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}

//添加用户
// @router /admin/user/add [post]
func (this *UserController) AddUser() {
	var usr models.User
	var code int
	var msg string

	body := this.Ctx.Input.RequestBody
	//绑定到User模型
	_ = json.Unmarshal(body, &usr)

	msg, code = validator.Validate(&usr)
	if code != errmsg.SUCCSE {
		result := map[string]interface{}{
			"status":  code,
			"message": msg,
		}
		this.Data["json"] = result
		this.ServeJSON() //返回json格式
		return
	}

	code = models.CheckUser(usr.Username)
	// 用户已存在
	if code == errmsg.ERROR_USERNAME_USED {
		code = errmsg.ERROR_USERNAME_USED
	}
	// 用户不存在
	if code == errmsg.SUCCSE {
		// 执行创建新用户
		models.CreateUser(&usr)
	}

	result := map[string]interface{}{
		"status":  code,
		"data":    usr,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式

}



//重置密码
// @router /admin/user/:id/reset [put]
func (this *UserController) ResetPwd() {
	id,_ := this.GetInt(":id")
	code := models.ResetPwd(id)
	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}


//编辑用户
// @router /admin/useredit/:id [put]
func (this *UserController) EditUser() {
	var usr models.User

	id, _ := this.GetInt(":id")

	body := this.Ctx.Input.RequestBody
	//绑定到User模型
	_ = json.Unmarshal(body, &usr)

	code := models.CheckUpUser(id, usr.Username)

	// 用户已存在
	if code == errmsg.ERROR_USERNAME_USED {
		this.Abort(string(errmsg.ERROR_USERNAME_USED))
	}
	// 用户不存在，执行更新
	if code == errmsg.SUCCSE {
		models.EditUser(id, &usr)
	}

	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}


//修改密码
// @router /admin/changepwd/:id [put]
func (this *UserController) ChangePwd(){
	var usr models.User

	id, _ := this.GetInt(":id")

	body := this.Ctx.Input.RequestBody
	//绑定到User模型
	_ = json.Unmarshal(body, &usr)

	code := models.ChangePwd(id, &usr)

	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}