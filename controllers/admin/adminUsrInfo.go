package admin

import (
	"beegoProject/models"
	"beegoProject/utils/errmsg"
	"beegoProject/utils/validator"
	"encoding/json"
	"github.com/astaxie/beego"
)

type AdminUsrInfoController struct {
	beego.Controller
}


//获取一个管理员信息
// @router /admin/userinfo/:id [get]
func (this *AdminUsrInfoController) GetOneAdminUsrsInfo() {
	id, _ := this.GetInt(":id")
	info, code := models.GetAdminUsrInfo(id)

	result := map[string]interface{}{
		"status":  code,
		"data":    info,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}



//获取所有管理员信息
// @router /admin/usersinfo [get]
func (this *AdminUsrInfoController) GetAdminUsrsInfo() {
	infos, _ := models.GetAdminUsrInfoList()
	code := errmsg.SUCCSE

	result := map[string]interface{}{
		"status":  code,
		"data":    infos,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}


//添加管理员信息
// @router /admin/userinfo/add [post]
func (this *AdminUsrInfoController) AddAdminUsrsInfo() {
	var adminuserinfo models.AdminUsrInfo
	var code int
	var msg string

	body := this.Ctx.Input.RequestBody
	//绑定到AdminUserInfo模型
	_ = json.Unmarshal(body, &adminuserinfo)

	msg, code = validator.Validate(&adminuserinfo)
	if code != errmsg.SUCCSE {
		result := map[string]interface{}{
			"status":  code,
			"message": msg,
		}
		this.Data["json"] = result
		this.ServeJSON() //返回json格式
		return
	}

	models.CreateAdminUserInfo(&adminuserinfo)
	result := map[string]interface{}{
		"status":  code,
		"data":    adminuserinfo,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式

}


//编辑管理员信息
// @router /admin/userinfoedit/:id [put]
func (this *AdminUsrInfoController) EditAdminUsrsInfo() {
	var info models.AdminUsrInfo

	id, _ := this.GetInt(":id")

	body := this.Ctx.Input.RequestBody
	_ = json.Unmarshal(body, &info)

	code := models.EditAdminUserInfo(id, &info)

	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}



//删除管理员信息
// @router /admin/userinfodelete/:id [delete]
func (this *AdminUsrInfoController) DeleteAdminUsrsInfo() {
	id, _ := this.GetInt(":id")

	code := models.DeleteAdminUsrInfo(id)
	result := map[string]interface{}{
		"status":  code,
		"message": errmsg.GetErrmsg(code),
	}
	this.Data["json"] = result
	this.ServeJSON() //返回json格式
}