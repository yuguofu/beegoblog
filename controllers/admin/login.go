package admin

import (
	"beegoProject/middleware"
	"beegoProject/models"
	"beegoProject/utils/errmsg"
	"encoding/json"
	"github.com/astaxie/beego"
)

type LoginController struct {
	beego.Controller
}


//后台登录页显示
// @router /admin/login [get]
func (this *LoginController) AdminLoginIndex() {
	this.TplName = "admin/index.html"
}

//登录提交
// @router /admin/login [post]
func (this *LoginController) AdminLogin() {
	var user models.User
	var code int
	var token string

	body := this.Ctx.Input.RequestBody
	_ = json.Unmarshal(body, &user)

	code = models.CheckLogin(user.Username, user.Password)
	if code == errmsg.SUCCSE {
		token, code = middleware.SetToken(user.Username)
	}

	result := map[string]interface{}{
		"status":    code,
		"message": errmsg.GetErrmsg(code),
		"token":   token,
	}
	this.Data["json"] = result

	this.ServeJSON() //返回json格式
}

