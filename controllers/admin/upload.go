package admin

import (
	"beegoProject/models"
	"beegoProject/utils/errmsg"
	"github.com/astaxie/beego"
)

type UploadController struct {
	beego.Controller
}

func (this *UploadController) Upload() {
	file, fileHeader, _ := this.Ctx.Request.FormFile("file")

	fielSize := fileHeader.Size

	url,code := models.UploadFile(file,fielSize)

	result := map[string]interface{}{
		"status": code,
		"message": errmsg.GetErrmsg(code),
		"url" : url,
	}
	this.Data["json"] = result

	this.ServeJSON() //返回json格式
}