package controllers

import "github.com/astaxie/beego"

type BaseController struct {
	beego.Controller
}

func (this *BaseController) Prepare()  {
	//获取访问路径
	this.Data["Path"] = this.Ctx.Request.RequestURI
}