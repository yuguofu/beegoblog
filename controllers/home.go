package controllers

import (
	"beegoProject/models"
	"strings"
)

type IndexController struct {
	BaseController
}

//默认分页大小
const defaultPagesize = 6

//首页
// @router / [get]
func (this *IndexController) Index() {
	keywords := this.GetString("keywords")
	pageSize, err := this.GetInt("pagesize")
	if pageSize < 1 || err != nil {
		pageSize = defaultPagesize
	}
	pageNum, err := this.GetInt("pagenum")
	if pageNum < 1 || err != nil {
		pageNum = 1
	}

	//文章列表
	articles, _, _ := models.GetArticles(keywords, pageSize, pageNum, "updated_at desc")
	//最热文章
	hotarts, _ := models.GetHotArticles(5)
	//获取管理员信息
	adminUsrInfo, _ := models.GetAdminUsrInfo(1)
	//获取友情链接
	friendlyLinks, _ := models.GetFriendlyLinks()

	this.Data["Articles"] = articles
	this.Data["Hotarts"] = hotarts
	this.Data["AdminUsrInfo"] = adminUsrInfo
	this.Data["FriendlyLinks"] = friendlyLinks

	this.TplName = "index.html"
}

//文章页
// @router /home/article [get]
// @router /home/article [post]
func (this *IndexController) Article() {
	keywords := strings.TrimSpace(this.GetString("keywords", ""))
	pageSize, err := this.GetInt("pagesize")
	if pageSize < 1 || err != nil {
		pageSize = defaultPagesize
	}
	pageNum, err := this.GetInt("pagenum")
	if pageNum < 1 || err != nil {
		pageNum = 1
	}

	//文章列表
	articles, _, _ := models.GetArticles(keywords, pageSize, pageNum, "updated_at desc")

	//分类列表
	categories, _ := models.GetAllCategory()
	//最热文章
	hotarts, _ := models.GetHotArticles(6)

	//页数
	var pageCount int
	if keywords == "" {
		//进入文章专栏时页数
		pageCount = models.GetArticlePageCount(-1, pageSize)
	} else {
		//关键字搜索时的页数
		pageCount = models.GetArticlePageCountOfSearch(keywords, pageSize)
	}

	//上一页和下一页中间的起始页、结束页配置---------------
	//设置开始页码，（共显示5个页码）
	var pageStart int
	if pageNum-2 > 0 {
		pageStart = pageNum - 2
	} else {
		pageStart = 1
	}
	//设置结束页码
	var pageEnd int
	if pageNum+2 > pageCount {
		pageEnd = pageCount
	} else {
		pageEnd = pageStart + 4
	}
	pStartToEnd := []int{}
	for i := pageStart; i <= pageEnd; i++ {
		pStartToEnd = append(pStartToEnd, i)
	}
	//-----------------------------------------------

	this.Data["Articles"] = articles
	this.Data["Categories"] = categories
	this.Data["Hotarts"] = hotarts
	this.Data["PageNum"] = pageNum
	this.Data["PageCount"] = pageCount
	this.Data["PageStart"] = pageStart
	this.Data["PageEnd"] = pageEnd
	this.Data["pStartToEnd"] = pStartToEnd

	this.TplName = "article.html"
}

//分类下的文章列表
// @router /home/category/:id/artlist [get]
func (this *IndexController) CateArtList() {
	cateId, _ := this.GetInt(":id")
	pageSize, err := this.GetInt("pagesize")
	if pageSize < 1 || err != nil {
		pageSize = 5
	}
	pageNum, err := this.GetInt("pagenum")
	if pageNum < 1 || err != nil {
		pageNum = 1
	}

	//分类下的文章
	articles, _, _ := models.GetCateArt(cateId, pageSize, pageNum)
	cate, _ := models.GetCategory(cateId)

	//分类列表
	categories, _ := models.GetAllCategory()

	//分类下的文章页数
	pageCount := models.GetArticlePageCount(cateId, pageSize)

	//上一页和下一页中间的起始页、结束页配置---------------
	//设置开始页码，（共显示5个页码）
	var pageStart int
	if pageNum-2 > 0 {
		pageStart = pageNum - 2
	} else {
		pageStart = 1
	}
	//设置结束页码
	var pageEnd int
	if pageNum+2 > pageCount {
		pageEnd = pageCount
	} else {
		pageEnd = pageStart + 4
	}
	pStartToEnd := []int{}
	for i := pageStart; i <= pageEnd; i++ {
		pStartToEnd = append(pStartToEnd, i)
	}
	//-----------------------------------------------

	this.Data["Articles"] = articles
	this.Data["Cate"] = cate //分类id对应的分类信息
	this.Data["Categories"] = categories
	this.Data["PageNum"] = pageNum
	this.Data["PageCount"] = pageCount
	this.Data["PageStart"] = pageStart
	this.Data["PageEnd"] = pageEnd
	this.Data["pStartToEnd"] = pStartToEnd

	this.TplName = "cateartlist.html"
}

//文章归档
// @router /home/archive [get]
func (this *IndexController) Archive() {
	archiveArticles := models.ArchiveArticle()
	//fmt.Println("atrMapList内容为：", archiveArticles)

	this.Data["ArchiveArticles"] = archiveArticles

	this.TplName = "archive.html"
}

//资源分享
// @router /home/resource [get]
//func (this *IndexController) Resource() {
//
//	this.TplName = "resource.html"
//}

//关于页
// @router /home/about [get]
func (this *IndexController) About() {
	//获取管理员信息
	adminUsrInfo, _ := models.GetAdminUsrInfo(1)
	//获取友情链接
	friendlyLinks, _ := models.GetFriendlyLinks()

	this.Data["AdminUsrInfo"] = adminUsrInfo
	this.Data["FriendlyLinks"] = friendlyLinks
	this.TplName = "about.html"
}
