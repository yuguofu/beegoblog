package controllers

import (
	"beegoProject/models"
	"github.com/russross/blackfriday/v2"
	"html/template"
)

type ArticleDetailController struct {
	BaseController
}



//文章详情页
// @router /home/article/:id [get]
func (this *ArticleDetailController) ArticleDetail() {
	id, _ := this.GetInt(":id")
	article, _ := models.GetOneArticle(id)
	mddata := []byte(article.Content)
	article.Content = string(template.HTML(blackfriday.Run(mddata)))


	//更新浏览量
	models.UpdatePageView(int(article.ID))

	this.Data["Article"] = article

	this.TplName = "detail.html"
}
